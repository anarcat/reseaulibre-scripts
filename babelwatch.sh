#!/bin/sh

# Configuration file
# Format:
#
# wiki /home/user/wiki.reseaulibre.ca
# node-a hostname-a.example.org
# node-b hostname-b.example.org
#
configfile="$HOME/.babelwatch"

grep -v '#' "${configfile}" | awk -v wiki="~/FOOwiki.reseaulibre.ca" '
    {
        # By default, will search for the wiki in this directory.
        # It will be configured by config file.

        if ($1 == "wiki") {
            wiki=$2;
            cmd="cd " $2 "/ && git pull";
            print "Wiki:", wiki;
            system(cmd);
            close(cmd);
        }
        else {
            if (! $3) {
                $3="root";
            }

            print "node:", $1, "host:", $2, "login:", $3;

            # empty the file: nodes/foonode/babelstatus
            outfile=wiki "/nodes/" $1 "/babelstatus";

            system("rm " outfile);
            system("touch " outfile);

            # executes: ssh example.org -l root babelstatus.sh
            cmd = "ssh " $2 " -l " $3 " babelstatus.sh";

            while ( ( cmd | getline result ) > 0 ) {
                # Removes metrics that often change. We monitor routes, not their quality.
                gsub(/(rxcost|txcost|cost|reach|metric|refmetric) [a-zA-Z0-9]+\s?/, "", result);

                # If babel is down, nc will say Connection refused.
                # Filter it out, so that we can more easily detect that the node is down.
                gsub(/nc: .* Connection refused/, "", result);
                printf result "\n" >> outfile;
            }

            # If the file is empty, we assume that the node is down.
            if (system("test -s " outfile) == 1) {
                printf "node is down" >> outfile;
                system("cd " wiki " && git commit -m \"node is down\" " outfile);
            }
        }
    }
    END {
        # Empty files have already been committed, so other changes are for routes
        # we do only one commit to avoid polluting the log even more.
        system("cd " wiki " && git commit -m \"route change detected\" -a");

        # Fixme: would be nice to push only if we committed stuff
        system("cd " wiki " && git push");
    }'

