#
# Copyright (C) 2012 OpenWrt.org
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

# Substituted by SDK, do not remove
# REVISION:=x

PKG_CONFIG_DEPENDS += \
	CONFIG_RL_MODE \
	CONFIG_RL_NUMBER \
	CONFIG_RL_REPO 

RL_NUMBER:=$(call qstrip,$(CONFIG_RL_NUMBER))
RL_NUMBER:=$(if $(RL_NUMBER),$(RL_NUMBER),$(REVISION))

RL_REPO:=$(call qstrip,$(CONFIG_RL_REPO))
RL_REPO:=$(if $(RL_REPO),$(RL_REPO),http://packages.reseaulibre.ca:8080/Routeurs/reseaulibre-openwrt/%W/%T/packages)

RL_SED:=$(SED) 's,%P,$(RL_REPO),g' \
	-e 's,%W,$(RL_NUMBER),g' \
